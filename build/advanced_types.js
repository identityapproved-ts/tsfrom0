"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
function logId(id) {
    if (typeof id === 'string') {
        console.log(id);
    }
    else if (typeof id === 'number') {
        console.log(id);
    }
    else {
        console.log(id);
    }
    ;
}
;
logId(1);
logId("stringa");
logId(true);
function logErrors(err) {
    if (Array.isArray(err)) {
        console.log(err);
    }
    else {
        console.log(err);
    }
}
;
function logObj(obj) {
    if ('a' in obj) {
        console.log("~ obj.a", obj.a);
    }
    else {
        console.log("~ obj.b", obj.b);
    }
}
;
function logMultiplesIds(a, b) {
    if (a === b) {
    }
    else {
        console.log(a);
    }
}
;
function fetchWithAuth(url, method) {
    return 23;
}
;
fetchWithAuth('urk', 'post');
let method = 'post';
fetchWithAuth('urk', method);
function authFetch(url, method) {
    return 23;
}
;
authFetch('urk', 'post');
const oldUser = {
    id: 23,
    firstName: 'id',
    lastName: 'app',
    city: 'Kyiv',
    age: 29,
    skills: [
        'front',
        'back'
    ]
};
;
;
const newUser = {
    id: 23,
    firstName: 'id',
    lastName: 'app',
    city: 'Kyiv',
    age: 29,
    skills: [
        'front',
        'back'
    ],
    createdAt: new Date(),
    log(id) { return ''; }
};
;
;
const ichigo = {
    age: 23,
    bankai: ''
};
;
const optUser = {
    login: 'jojo@gm.cm',
    password: '23232323'
};
function multiply(first = 5, second) {
    if (!second) {
        return first * first;
    }
    return first * second;
}
;
console.log(multiply(10, 2));
;
function testPassword(user) {
    var _a;
    const t = (_a = user.password) === null || _a === void 0 ? void 0 : _a.type;
}
;
function testing(param) {
    const p = param !== null && param !== void 0 ? param : multiply();
}
;
function lodSomething(smth) {
    console.log(smth);
}
;
const voidLog = lodSomething(1);
function mult(frst, scnd) {
    if (!scnd) {
        return frst ** frst;
    }
    return frst ** scnd;
}
;
mult(2, 2);
const stands = ["Star Platinum", "The World", "Silver Chariot", "Stone Ocean"];
const sortedList = {
    s: ['s']
};
stands.sort().forEach((stand) => sortedList.s.push(stand));
console.log("~ sortedList", sortedList);
let input;
input = 3;
input = ['arr', 'ray'];
function run(i) {
    if (typeof i === 'number') {
        i++;
    }
    else {
        i;
    }
}
;
function getSmth() {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield fetch('');
        }
        catch (err) {
            if (err instanceof Error) {
                console.log(err);
            }
        }
    });
}
;
function generateError(message) {
    throw new Error(message);
}
;
function dumpError() {
    while (true) {
    }
}
;
function recurs() {
    return recurs();
}
;
function switchProcess(action) {
    switch (action) {
        case 'commit':
            break;
        case 'checkout':
            break;
        default:
            const _ = action;
            throw new Error(action);
    }
}
;
function isString(x) {
    if (typeof x === 'string') {
        return true;
    }
    else if (typeof x === 'number') {
        return false;
    }
    ;
    generateError(x);
}
;
const n = null;
const n1 = null;
function getUser() {
    if (Math.random() > 0.5) {
        return null;
    }
    else {
        return {
            name: 'Dick'
        };
    }
}
;
const user = getUser();
if (user) {
    const namename = user.name;
}
let a = 5;
let b = a.toString();
let c = new String(a).valueOf();
let d = new Boolean(a).valueOf();
const bestUser = {
    name: 'Jonathan Joestar',
    email: 'bizzareadventure@jojo.com',
    login: 'Jojo'
};
;
const admin = Object.assign(Object.assign({}, bestUser), { role: 1 });
function userToAdmin(user) {
    return {
        name: user.name,
        role: 1
    };
}
;
function log(id) {
    if (isAString(id)) {
        console.log(id);
    }
    else {
        console.log(id);
    }
}
function isAString(x) {
    return typeof x === 'string';
}
function isAdmin(user) {
    return 'role' in user;
}
;
function isAdminAlternative(user) {
    return user.role !== undefined;
}
;
function setRole(user) {
    if (isAdmin(user)) {
        user.role = 0;
    }
    else {
        throw new Error("User is not an Admin");
    }
}
//# sourceMappingURL=advanced_types.js.map