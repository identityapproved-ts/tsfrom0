"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, state, value, kind, f) {
    if (kind === "m") throw new TypeError("Private method is not writable");
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a setter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
    return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
};
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
};
var _Vehicle_price;
class OopUser {
    constructor(ageOrName, age) {
        if (typeof ageOrName === "string") {
            this.name = ageOrName;
        }
        else if (typeof ageOrName === "number") {
            this.age = ageOrName;
        }
        if (typeof age === "number") {
            this.age = age;
        }
    }
}
const userClass = new OopUser("Jojo");
const userClass1 = new OopUser();
const userClass2 = new OopUser(23);
const userClass3 = new OopUser("Kurotsuchi", 32);
var PaymentsStatus;
(function (PaymentsStatus) {
    PaymentsStatus[PaymentsStatus["Holded"] = 0] = "Holded";
    PaymentsStatus[PaymentsStatus["Processed"] = 1] = "Processed";
    PaymentsStatus[PaymentsStatus["Reversed"] = 2] = "Reversed";
})(PaymentsStatus || (PaymentsStatus = {}));
class Payment {
    constructor(id) {
        this.status = PaymentsStatus.Holded;
        this.createdAt = new Date();
        this.id = id;
    }
    getPaymentLifeTime() {
        return new Date().getTime() - this.createdAt.getTime();
    }
    unholdPayment() {
        if (this.status == PaymentsStatus.Processed) {
            throw new Error("Can't give you money");
        }
        this.status = PaymentsStatus.Reversed;
        this.updatedAt = new Date();
    }
}
const payment = new Payment(1);
payment.unholdPayment();
console.log("~ payment", payment);
const time = payment.getPaymentLifeTime();
console.log("~ time", time);
class userSetOrGet {
    set login(l) {
        this._login = "little" + l;
        this.createdAt = new Date();
    }
    get login() {
        return "undefined";
    }
    setPassword(p) {
        return __awaiter(this, void 0, void 0, function* () { });
    }
}
const userm = new userSetOrGet();
userm.login = "bird";
console.log(userm);
console.log(userm.login);
console.log(userm._login);
class Logger {
    log(...args) {
        console.log(...args);
    }
    error(...args) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log(...args);
        });
    }
}
class PayableUser {
    delete() {
        throw new Error("Method not implemented.");
    }
    pay(paymentId) {
    }
}
class PaymentExt {
    constructor(id) {
        this.status = "new";
        this.id = id;
    }
    pay() {
        this.status = "paid";
    }
}
class PersistendPayment extends PaymentExt {
    constructor() {
        const id = Math.random();
        super(id);
    }
    save() {
    }
    pay(date) {
        super.pay();
        if (date) {
            this.paidAt = date;
        }
    }
}
new PersistendPayment().pay(new Date());
class Write {
    constructor() {
        this.process = "write";
        console.log(this.process);
    }
}
class Read extends Write {
    constructor() {
        console.log("this.is.not.this");
        super();
        this.process = "read";
        console.log(this.process);
    }
}
new Read();
new Error("");
class HttpError extends Error {
    constructor(message, code) {
        super(message);
        this.code = code !== null && code !== void 0 ? code : 404;
    }
}
class Usr {
    constructor(name) {
        this.name = name;
    }
}
class Usrs extends Array {
    searchUser(name) {
        return this.filter((u) => u.name === "Jojo");
    }
    toString() {
        return this.map((u) => u.name).join(", ");
    }
}
const usrs = new Usrs();
usrs.push(new Usr("Jojo"));
usrs.push(new Usr("Root"));
console.log(usrs.toString());
class UsrsList {
    push(u) {
        return this.users.push(u);
    }
}
class PaymentComp {
}
class UserWithPayment {
    constructor(user, payment) {
        this.user = user;
        this.payment = payment;
    }
}
class Vehicle {
    constructor() {
        _Vehicle_price.set(this, void 0);
    }
    set model(m) {
        this._model = m;
        __classPrivateFieldSet(this, _Vehicle_price, 23, "f");
    }
    get model() {
        return this._model;
    }
    isPriceEqual(p) {
        return __classPrivateFieldGet(this, _Vehicle_price, "f") === __classPrivateFieldGet(p, _Vehicle_price, "f");
    }
    addDamage(damage) {
        this.damages.push(damage);
    }
}
_Vehicle_price = new WeakMap();
new Vehicle().mark = "Honda";
new Vehicle();
class Track extends Vehicle {
    setRun(km) {
        this.run = km / 0.62;
    }
}
class UserService {
    constructor(id) { }
    static getUser(id) {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
    create() {
        UserService.db;
    }
}
(() => {
    UserService.db = "mongo";
})();
UserService.getUser(23);
const inst = new UserService(23);
inst.create();
console.log("~ UserService", UserService);
class ThisPayment {
    constructor() {
        this.date = new Date();
        this.getDateArrow = () => this.date;
    }
    getDate() {
        return this.date;
    }
}
const p = new ThisPayment();
const thisUser = {
    id: 32,
    paymentDate: p.getDate.bind(p),
    paymentDateArrow: p.getDateArrow,
};
class PaymentPersistant extends ThisPayment {
    save() {
        return this.getDateArrow();
        return super.getDate();
    }
}
console.log(new PaymentPersistant().save());
class UserBuilder {
    setName(name) {
        this.name = name;
        console.log("~ this", this);
        return this;
    }
    isAdmin() {
        return this instanceof AdminBuilder;
    }
}
class AdminBuilder extends UserBuilder {
}
const usrBldr = new UserBuilder().setName("Naruto");
const usrBldr2 = new AdminBuilder().setName("Jotaro");
let usssr = new UserBuilder();
if (usssr.isAdmin()) {
    console.log("first", usssr);
}
else {
    console.log("first", usssr);
}
class Controller {
    handleWithLogs(req) {
        console.log("first");
        this.handle(req);
        console.log("second");
    }
}
class UserController extends Controller {
    handle(req) {
        console.log(req);
    }
}
const usrContr = new UserController();
usrContr.handleWithLogs("Request");
//# sourceMappingURL=classes.js.map