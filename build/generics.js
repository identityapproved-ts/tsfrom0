"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const arrr = [1, 2, 3];
function gen() {
    return __awaiter(this, void 0, void 0, function* () {
    });
}
const check = {
    drive: true,
    kpp: false
};
function logMiddleware(data) {
    console.log(data);
    return data;
}
const logRes = logMiddleware(10);
function splitHalf(data) {
    const l = data.length / 2;
    return data.splice(0, l);
}
splitHalf([1, 2, 3, 4, 5]);
function dataToString(data) {
    if (Array.isArray(data)) {
        return data.toString();
    }
    switch (typeof data) {
        case 'string':
            return data;
        case 'number':
        case 'boolean':
        case 'symbol':
        case 'bigint':
        case 'function':
            return data.toString();
        case 'object':
            return JSON.stringify(data);
        default:
            return undefined;
    }
}
console.log(dataToString(3));
console.log(dataToString(true));
console.log(dataToString(['a', 'b']));
console.log(dataToString({ a: 1 }));
const split = splitHalf;
const split2 = splitHalf;
const logLine = {
    timeStamp: new Date(),
    data: {
        a: 1
    }
};
class Venicle {
}
function kmToMiles(venicle) {
    venicle.run = venicle.run / 0.62;
    return venicle;
}
class LCV extends Venicle {
}
const venicle = kmToMiles(new Venicle());
const lcv = kmToMiles(new LCV());
class Resp {
    constructor(data, error) {
        if (data) {
            this.data = data;
        }
        if (error) {
            this.error = error;
        }
    }
}
new Resp('data', 0);
class HTTPResp extends Resp {
    setCode(code) {
        this.code = code;
    }
}
const resp2 = new HTTPResp();
class List {
    constructor(items) {
        this.items = items;
    }
}
class Accordion {
}
class ExtendedListClass extends List {
    first() {
        return this.items[0];
    }
}
function ExtendedList(Base) {
    return class ExtendedList extends Base {
        first() {
            return this.items[0];
        }
    };
}
class AccordionList {
    constructor(items) {
        this.items = items;
    }
}
const list = ExtendedList(AccordionList);
const ressss = new list(['first', 'second']);
console.log(ressss.first());
//# sourceMappingURL=generics.js.map