"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const sendedReq = {
    topicId: 23,
    status: "published"
};
var QuestionStatus;
(function (QuestionStatus) {
    QuestionStatus["Published"] = "published";
    QuestionStatus["Deleted"] = "deleted";
    QuestionStatus["Draft"] = "draft";
})(QuestionStatus || (QuestionStatus = {}));
;
function getFaqs(req) {
    return __awaiter(this, void 0, void 0, function* () {
        const res = yield fetch('/test2.json', {
            method: 'POST',
            body: JSON.stringify(req)
        });
        if (!res.ok) {
            throw new Error('Waaaaat?');
        }
        const data = yield res.json();
        return data;
    });
}
console.log(getFaqs(sendedReq));
//# sourceMappingURL=test2.js.map