"use strict";
;
;
;
;
;
;
function isSuccess(res) {
    if (res.status === TransactionStatus.Success) {
        return true;
    }
    return false;
}
function transactionStatus(res) {
    if (isSuccess(res)) {
        return res.data.databaseId;
    }
    else {
        throw new Error(res.data.errorMessage);
    }
}
//# sourceMappingURL=test4.js.map