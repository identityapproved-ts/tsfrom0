"use strict";
class Product {
    constructor(id, name, price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }
}
class Delivery {
    constructor(date) {
        this.date = date;
    }
}
class HomeDelivery extends Delivery {
    constructor(adress, date) {
        super(date);
        this.adress = adress;
        this.date = date;
    }
}
class ShopDelivery extends Delivery {
    constructor(shopId) {
        super(new Date());
        this.shopId = shopId;
    }
}
class Cart {
    constructor() {
        this.products = [];
    }
    addProduct(product) {
        this.products.push(product);
    }
    deleteProduct(productId) {
        this.products = this.products.filter((prod) => productId !== prod.id);
    }
    getSum() {
        return this.products
            .map((p) => p.price)
            .reduce((p1, p2) => p1 + p2);
    }
    setDelivery(delivery) {
        this.delivery = delivery;
    }
    checkOut() {
        if (this.products.length === 0) {
            throw new Error("Add some product, please");
        }
        if (!this.delivery) {
            throw new Error("Add delivery options, please");
        }
        return { success: true };
    }
}
const cart = new Cart();
cart.addProduct(new Product(1, "Tofu", 2300));
cart.addProduct(new Product(2, "Soya", 2300));
cart.addProduct(new Product(3, "Rice", 9200));
cart.deleteProduct(1);
cart.setDelivery(new HomeDelivery("Lake", new Date()));
console.log(cart.getSum());
console.log(cart.checkOut());
//# sourceMappingURL=test5.js.map