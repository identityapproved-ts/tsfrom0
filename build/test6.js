"use strict";
class AbsLogger {
    printDate(date) {
        this.logMessage(date.toString());
    }
}
class DateLog extends AbsLogger {
    logMessage(m) {
        console.log(m);
    }
    logWithDate(m) {
        this.printDate(new Date());
        this.logMessage(m);
    }
}
const logger = new DateLog();
logger.logWithDate("MESSAGE!");
//# sourceMappingURL=test6.js.map