"use strict";
const groupData = [
    { group: 1, name: 'a' },
    { group: 1, name: 'b' },
    { group: 1, name: 'b' },
    { group: 1, name: 'c' },
    { group: 2, name: 'a' },
    { group: 2, name: 'b' },
    { group: 2, name: 'c' },
];
function group(array, key) {
    return array.reduce((map, item) => {
        const itemKey = item[key];
        let currEl = map[itemKey];
        console.log("~ itemKey", itemKey);
        if (Array.isArray(currEl)) {
            currEl.push(item);
        }
        else {
            currEl = [item];
        }
        map[itemKey] = currEl;
        console.log("~ currEl", currEl);
        return map;
    }, {});
}
const grouppedRes = group(groupData, 'name');
console.log(grouppedRes);
//# sourceMappingURL=test8.js.map