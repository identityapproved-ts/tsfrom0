// union

function logId(id: string | number | boolean) {
   // narrowing
   if (typeof id === 'string') {
      console.log(id); 
   } else if (typeof id === 'number') {
      console.log(id); 
   } else {
      console.log(id);
      
   };
};

logId(1);
logId("stringa");
logId(true);

function logErrors(err:string | string[]) {
   if (Array.isArray(err)) {
      console.log(err);
   } else {
      console.log(err);
   }
};

function logObj(obj: { a: number} | { b:number }) {
   if ('a' in obj) {
      console.log("~ obj.a", obj.a)
   } else {
      console.log("~ obj.b", obj.b)      
   }
};

function logMultiplesIds(a: string | number, b: string | boolean) {
   if (a === b) {
      
   } else {
      console.log(a);
      
   }
};

// literal types

function fetchWithAuth(url: string, method: 'post' | 'get'): number {
   return 23;
};

fetchWithAuth('urk', 'post');

let method = 'post';
// const method = 'post';

fetchWithAuth('urk', method as 'post');

// alias

type httpMethod = 'post' | 'get';

function authFetch(url: string, method: httpMethod): number {
   return 23;
};

authFetch('urk', 'post');

type AliasUser = {
   firstName: string,
   lastName: string,
   city: string,
   age: number,
   skills: string[]
};


// intersection

type userId = {
   id: number;
//  city: string;
};

type userWithId = AliasUser & userId;

/* type userWithId = {
   user: AliasUser,
   id: userId
}; */


const oldUser: userWithId = {
   id: 23,
   firstName: 'id',
   lastName: 'app',
   city: 'Kyiv',
   age: 29,
   skills: [
      'front',
      'back'
   ]
};

// interfaces

interface UserInterface {
   firstName: string,
   lastName: string,
   city: string,
   age: number,
   skills: string[]

   log: (id: number) => string
};

interface UserCreated {
   createdAt: Date;
}

interface UserIdInterface extends UserInterface, UserCreated {
   id: number;
};

const newUser: UserIdInterface = {
   id: 23,
   firstName: 'id',
   lastName: 'app',
   city: 'Kyiv',
   age: 29,
   skills: [
      'front',
      'back'
   ],
   createdAt: new Date(),

   log(id){ return '' }
};

interface UserDictionaryInterface {
   [index: number]: UserInterface; // same in type
};

// type ud = Record<number, UserInterface>

// interface or types

interface Ichigo {
   age: number
};

// merging

interface Ichigo {
   bankai: string
}

const ichigo: Ichigo = {
   age: 23,
   bankai: ''
}

// optional

interface OptionalUser {
   login: string,
   password?: string // :? => property can not exist
};

const optUser: OptionalUser = {
   login: 'jojo@gm.cm',
   password: '23232323'
};

function multiply(first: number = 5, second?: number) {
   if (!second) {
      return first * first;
   }
   return first * second;
};

console.log(multiply(10,2));

interface UserPassType {
   login: string,
   password?: {
      type: 'primary' | 'secondary'
   }
};

function testPassword(user:UserPassType) {
   const t = user.password?.type; // === user.password ? user.password.type : undefined
};

function testing(param?:number) {
   const p = param ?? multiply();
};

// void

function lodSomething(smth: string | number): void {
   console.log(smth);
};

const voidLog = lodSomething(1);

function mult(frst: number, scnd?: number) {
   if (!scnd) {
      return frst ** frst;      
   }
   return frst ** scnd;
};

mult(2, 2);

type voidFunction = () => void;

const stands = ["Star Platinum", "The World", "Silver Chariot", "Stone Ocean"];

const sortedList = {
   s: ['s']
};

stands.sort().forEach((stand) => sortedList.s.push(stand));
console.log("~ sortedList", sortedList)

// unknown 

let input: unknown;

input = 3;
input = ['arr', 'ray'];

function run(i:unknown) {
   if (typeof i === 'number') {
      i++;
   } else {
      i;
   }
};

async function getSmth() {
   try {
     await fetch('')
   } catch (err) {
      // err is unknown
      if (err instanceof Error) {
         console.log(err);
      }
   }
};

type UnionUnknown = unknown | null; // => unknown
type intersectionUnknown = unknown & null; // => null, or any type

// never

function generateError(message:string): never {
   throw new Error(message);
};

function dumpError(): never {
   while (true) {
      
   }
};

function recurs(): never {
   return recurs();
};

// const a: never = null; cant assign 

type switchAction = 'commit' | 'checkout' // | 'push';

function switchProcess(action:switchAction) {
   switch (action) {
      case 'commit':
         //... 
         break;
   
      case 'checkout':
         //... 
         break;
      
      default:
         const _: never = action;
         throw new Error(action);
   }
};

function isString(x:string | number): boolean {
   if (typeof x === 'string') {
      return true
   } else if (typeof x === 'number'){
      return false
   }; // error => return undefined
   generateError(x)
};

// null

const n: null = null;
const n1: any = null;
// const n3: string = null;
// const n4: boolean = null;
// const n5: number = null;
// const n6: undefined = null;

interface IUser {
   name: string
}

function getUser() {
   if (Math.random() > 0.5) {
      return null;
   } else {
      return {
         name: 'Dick'
      } as IUser
   }
};

const user = getUser(); // can be undefined
if (user) {
   const namename = user.name;
}

// type conversion

let a = 5;
let b: string = a.toString();
let c: string /* String == wrapper object */ = new String(a).valueOf();
let d: boolean = new Boolean(a).valueOf();

interface User {
   name: string,
   email: string,
   login: string
}

const bestUser: User = {
   name: 'Jonathan Joestar',
   email: 'bizzareadventure@jojo.com',
   login: 'Jojo'
}; // as User

interface Admin {
   name: string,
   role: number
};

const admin: Admin = {
   ...bestUser,
   role:1 
}; // bad idea

function userToAdmin(user:User): Admin {
   return {
      name: user.name,
      role: 1
   }
};

// typeguard

function log(id:string | number) {
   if (isAString(id)) {
      console.log(id);      
   } else {
      console.log(id);
   }
}

function isAString(x: string | number): x is string {
   return typeof x === 'string';
}

function isAdmin(user:User|Admin): user is Admin /* ==> not boolean */ {
   return 'role' in user;
}; // can't be asynchronous

function isAdminAlternative(user:User|Admin): user is Admin {
   return (user as Admin).role !== undefined;
};

function setRole(user:User|Admin) {
   if (isAdmin(user)) {
      user.role = 0
   } else {
      throw new Error("User is not an Admin");
      
   }
}