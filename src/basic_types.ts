// #1

/* 
const revenue: number = 800;
const bonus: number = 200;
const str: string = "string";
const bool: boolean = true;

const res: number = revenue + bonus;
console.log(res)
 */

// #2 functions

/* 
function getFullName(userEntity: { firstName: string, lastName: string}): string{
   return `${userEntity.firstName} ${userEntity.lastName}`
}
// console.log(getFullName("true", "false"))

const getFullNameArrow = (firstName: string, lastName: string): string => {
   return `${firstName} ${lastName}`
};
 */

// #3 objects

/* 
const user = {
   firstName: 'id',
   lastName: 'app',
   city: 'Kyiv',
   age: 29,
   skills: {
      front: true,
      back: true
   }
};

console.log(getFullName(user))
 */

// #4 arrays
/* 
const skills: string[] = ['JS', 'React', 'Node'];

for (const skill of skills) {
   console.log(skill.toUpperCase())
};

const mapped = skills.map((s: string) => s + "!");
console.log("~ mapped", mapped)
 */
// #5 tuples
/* 
const tuples: [number, string] = [23, 'Redux'];
// const id = tuples[0];
// const skillName = tuples[1];
// const errorEl = tuples[2];

const [id, skillName] = tuples;

const arr: [number, string, ...boolean[]] = [1, 'spread', true, false];
 */
// #6 reaonly
/* 
const readOnlySkills: readonly [number, string] = [23, 'Linux'];
// readOnlySkills[0] = 13;

const equalReadOnly: ReadonlyArray<string> = ['sometimes', 'not classic'];
 */
// #7 enums
/* 
enum Message {
   OK = 200,
   BAD_REQUEST = 400,
   NOT_FOUND = 404,
   INTERNAL_SERVER_ERROR = 500,
   DURAK = 'ponyal'
}

const res = {
   message: Message.OK,
   statusCode: 200
};

if (res.message === Message.OK) {
   
}

function action(m: Message) {
   console.log(m);
};

action(Message.NOT_FOUND);
action(500);
 */

const compute = () => 32;

enum Roles {
   admin = 23,
   user = compute()
};

function test(a: { admin: number, user: number }) {
   console.log(a.admin)
   console.log(a.user)
};

test(Roles);

const enum RolesCnst {
   admin = 23,
   user = 666
};

const res = RolesCnst.user;