class OopUser {
  name: string;
  age: number;

  // overload
  constructor();
  constructor(name: string);
  constructor(age: number);
  constructor(name: string, age: number);
  constructor(ageOrName?: string | number, age?: number) {
    if (typeof ageOrName === "string") {
      this.name = ageOrName;
    } else if (typeof ageOrName === "number") {
      this.age = ageOrName;
    }

    if (typeof age === "number") {
      this.age = age;
    }
  }
}

const userClass = new OopUser("Jojo");
const userClass1 = new OopUser();
const userClass2 = new OopUser(23);
const userClass3 = new OopUser("Kurotsuchi", 32);
// console.log(userClass);
// userClass.name = 'Naruto';
// console.log(userClass);

/* class OopAdmin {
   role: number;
}

const adminClass = new OopAdmin();
admin.role = 1 */

// part. methods

enum PaymentsStatus {
  Holded,
  Processed,
  Reversed,
}

class Payment {
  id: number;
  status: PaymentsStatus = PaymentsStatus.Holded; // change order of assigment
  createdAt: Date = new Date();
  updatedAt: Date;

  constructor(id: number) {
    this.id = id;
    //  this.createdAt = new Date();
    //  this.status = PaymentsStatus.Holded;
  }

  getPaymentLifeTime(): number {
    return new Date().getTime() - this.createdAt.getTime();
  }

  unholdPayment(): void {
    if (this.status == PaymentsStatus.Processed) {
      throw new Error("Can't give you money");
    }
    this.status = PaymentsStatus.Reversed;
    this.updatedAt = new Date();
  }
}

const payment = new Payment(1);
payment.unholdPayment();
console.log("~ payment", payment);

const time = payment.getPaymentLifeTime();
console.log("~ time", time);

// overload metods

/* class OverloadedUserMetod {
  skills: string[];

  addSkill(newSkill: string): void;
  addSkill(newSkills: string[]): void;
  addSkill(newSkillOrSkills: string | string[]): void {
    if (typeof newSkillOrSkills == "string") {
      this.skills.push(newSkillOrSkills);
    } else {
      this.skills.concat(newSkillOrSkills);
    }
  }
}

new OverloadedUserMetod().addSkill("The World");

function runOver(distance: string): string;
function runOver(distance: number): number;
function runOver(distance: number | string): number | string {
  if (typeof distance == "number") {
    return 23;
  } else {
    return "marathon";
  }
}

runOver("looong"); */

// part. getters | setters

class userSetOrGet {
  _login: string;
  password: string;
  createdAt: Date;

  // without setter _login = readonly
  set login(l: string) {
    this._login = "little" + l;
    // side effect
    this.createdAt = new Date();
  }
  // if not declare type of setter ==> automaticly = string, of same as return type in getter
  get login(): string {
    return "undefined";
  }

  // setters or getters can be only sync ==> use methods
  async setPassword(p: string) { }
}

const userm = new userSetOrGet();
userm.login = "bird";
console.log(userm);
console.log(userm.login);
console.log(userm._login);

// implements

interface ILogger {
  log(...args: any[]): void;
  error(...args: any[]): void;
}

class Logger implements ILogger {
  log(...args: any[]): void {
    console.log(...args);
  }
  async error(...args: any[]): Promise<void> {
    // throw
    console.log(...args);
  }
}

//............

interface IPayable {
  pay(paymentId: number): void;
  price?: number;
}

interface IDeleteble {
  delete(): void;
}

class PayableUser implements IPayable, IDeleteble {
  delete(): void {
    throw new Error("Method not implemented.");
  }
  pay(paymentId: number): void {
    //...
  }
  price?: number; // | undefined;
}

// part. extends

type PaymentStatus = "new" | "paid";

class PaymentExt {
  id: number;
  status: PaymentStatus = "new";

  constructor(id: number) {
    this.id = id;
  }

  pay() {
    this.status = "paid";
  }
}

class PersistendPayment extends PaymentExt {
  databaseId: number;
  paidAt: Date;

  constructor() {
    const id = Math.random();
    super(id);
  }

  save() {
    // save in base
  }

  override pay(date?: Date) {
    super.pay(); // this.status = 'paid'; <== override method
    if (date) {
      this.paidAt = date;
    }
  }
}

new PersistendPayment().pay(new Date());

// part. features of inheritance

class Write {
  process: string = "write";

  constructor() {
    console.log(this.process);
  }
}

class Read extends Write {
  override process: string = "read";

  constructor() {
    console.log("this.is.not.this");
    super();
    console.log(this.process);
  }
}

new Read();
new Error("");

class HttpError extends Error {
  code: number;

  constructor(message: string, code?: number) {
    super(message);
    this.code = code ?? 404;
  }
}

// part. inheritance vs composition

class Usr {
  name: string;

  constructor(name: string) {
    this.name = name;
  }
}

// inheritance
class Usrs extends Array<Usr> {
  searchUser(name: string) {
    return this.filter((u) => u.name === "Jojo");
  }

  override toString(): string {
    return this.map((u) => u.name).join(", ");
  }
}

const usrs = new Usrs();
usrs.push(new Usr("Jojo"));
usrs.push(new Usr("Root"));
console.log(usrs.toString());

// composition

class UsrsList {
  users: Usr[];

  push(u: Usr) {
    return this.users.push(u);
  }
}
// ...
class PaymentComp {
  date: Date;
}
/* 
class UserWithPayment extends PaymentComp {
  name: string;
} // complication of code coherence, and property conflicts
*/

class UserWithPayment {
  user: Usr;
  payment: PaymentComp;

  constructor(user: Usr, payment: PaymentComp) {
    this.user = user;
    this.payment = payment;
  }
}

// part. public/private prop

class Vehicle {
  mark: string; // same as => public mark: string;
  private damages: string[];
  private _model: string;
  protected run: number; // available in extended classes
  #price: number; // same as private in JS

  set model(m: string) {
    this._model = m;
    this.#price = 23;
  }

  get model(): string {
    return this._model;
  }

  isPriceEqual(p: Vehicle) {
    return this.#price === p.#price;
  }

  addDamage(damage: string) {
    this.damages.push(damage);
  }
}

new Vehicle().mark = "Honda";
new Vehicle();

class Track extends Vehicle {
  setRun(km: number) {
    this.run = km / 0.62;
    // this.price - err
    // this.damage - error
  }
}

// part. static props

class UserService {
  // static name: string; // with static - "name" build-in property
  static property: number;
  private static db: any;

  static async getUser(id: number) {
    // return await UserService.db.findById(id);
  }

  constructor(id: number) { }

  create() {
    UserService.db;
  }

  static {
    // can be only synchronously
    // can't use await
    UserService.db = "mongo";
  }
}

UserService.getUser(23);
const inst = new UserService(23);
inst.create();
console.log("~ UserService", UserService);

// part.  this / context

class ThisPayment {
  private date: Date = new Date();

  getDate(this: ThisPayment) {
    return this.date;
  }

  getDateArrow = () => this.date; // be careful with inheritance
}

const p = new ThisPayment();

const thisUser = {
  id: 32,
  paymentDate: p.getDate.bind(p),
  paymentDateArrow: p.getDateArrow,
};
/* 
console.log("p.getDate", p.getDate());
console.log("thisUser.paymentDate()", thisUser.paymentDate());
console.log("thisUser.paymentDateArrow()", thisUser.paymentDateArrow());
 */
class PaymentPersistant extends ThisPayment {
  save() {
    return this.getDateArrow(); // super.getDateArrow(); runtime error
    return super.getDate();
  }
}

console.log(new PaymentPersistant().save());

// part.  typing this

class UserBuilder {
  name: string;

  setName(name: string): this {
    this.name = name;
    console.log("~ this", this);
    return this;
  }

  isAdmin(): this is AdminBuilder {
    return this instanceof AdminBuilder;
  }
}

class AdminBuilder extends UserBuilder {
  role: string[]; // for correct work of typeguard, it is necessary that the objects differ
}

const usrBldr = new UserBuilder().setName("Naruto");
const usrBldr2 = new AdminBuilder().setName("Jotaro");

let usssr: UserBuilder | AdminBuilder = new UserBuilder();

if (usssr.isAdmin()) {
  console.log("first", usssr);
} else {
  console.log("first", usssr);
}

// part.  abstract classes

abstract class Controller {
  abstract handle(req: any): void;

  handleWithLogs(req: any) {
    console.log("first");
    this.handle(req);
    console.log("second");
  }
}

class UserController extends Controller {
  handle(req: any): void {
    console.log(req);
  }
}

// new Controller() - error
const usrContr = new UserController();
usrContr.handleWithLogs("Request");
