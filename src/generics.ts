// part. build-in generics

const arrr: Array<number> = [1, 2, 3];

async function gen() {
   // const a = await new Promise<number>((resolve, reject) => {
   //    resolve(1);
   // });
}

const check: Record<string, boolean> = {
   drive: true,
   kpp: false
};

// part.  generic functions

function logMiddleware<T>(data: T): T {
   console.log(data);
   return data;
}

const logRes = logMiddleware<number>(10);

function splitHalf<T>(data: Array<T>): Array<T> {
   const l = data.length / 2;
   return data.splice(0, l);
}

splitHalf<number>([1, 2, 3, 4, 5]);

// !TASK toString function

function dataToString<T>(data: T): string | undefined {
   if (Array.isArray(data)) {
      return data.toString();
   }
   switch (typeof data) {
      case 'string':
         return data;
      case 'number':
      case 'boolean':
      case 'symbol':
      case 'bigint':
      case 'function':
         return data.toString();
      case 'object':
         return JSON.stringify(data);
      default:
         return undefined;
   }
}

console.log(dataToString(3));
console.log(dataToString(true));
console.log(dataToString(['a', 'b']));
console.log(dataToString({ a: 1 }));

// part. using in types

const split: <T>(data: Array<T>) => Array<T> = splitHalf;
const split2: <Y>(data: Array<Y>) => Array<Y> = splitHalf;


interface ILogLine<T> {
   timeStamp: Date,
   data: T;
}

type LogLineType<T> = {
   timeStamp: Date;
   data: T;
};

const logLine: ILogLine<{ a: number; }> = {
   timeStamp: new Date(),
   data: {
      a: 1
   }
};

// part. generics limitations

class Venicle {
   run: number;
}

function kmToMiles<T extends Venicle>(venicle: T): T {
   venicle.run = venicle.run / 0.62;
   return venicle;
}

class LCV extends Venicle {
   capacity: number;
}

const venicle = kmToMiles(new Venicle());
const lcv = kmToMiles(new LCV());
// kmToMiles({a: 1}) // err

/* function logId<T extends string | number, Y>(id: T, additionalData: Y): { id: T, data: Y; } {
   console.log(id);
   console.log(additionalData);
   return { id, data: additionalData };

} */

// part. generics in classes

class Resp<D, E>{
   data?: D;
   error?: E;

   constructor(data?: D, error?: E) {
      if (data) {
         this.data = data;
      }
      if (error) {
         this.error = error;
      }
   }
}

new Resp<string, number>('data', 0);

class HTTPResp<F> extends Resp<string, number>{
   code: F;

   setCode(code: F) {
      this.code = code;
   }
}

const resp2 = new HTTPResp();

// part. mixins

type Consstructor = new (...args: any[]) => {};
type GConsstructor<T = {}> = new (...args: any[]) => T;

class List {
   constructor(public items: string[]) { }
}

class Accordion {
   isOpened: boolean;
}

type ListType = GConsstructor<List>;
type AccordionType = GConsstructor<Accordion>;

class ExtendedListClass extends List {
   first() {
      return this.items[0];
   }
}

function ExtendedList<TBase extends ListType & AccordionType>(Base: TBase) {
   return class ExtendedList extends Base {
      first() {
         return this.items[0];
      }
   };
}

class AccordionList {
   isOpened: true;

   constructor(public items: string[]) { }
}

const list = ExtendedList(AccordionList);
const ressss = new list(['first', 'second']);
console.log(ressss.first());