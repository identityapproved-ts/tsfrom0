// part. keyof

interface IUser {
   name: string;
   age: number;
}

type KeysOfUser = keyof IUser;

const key: KeysOfUser = 'age';

function getValue<T, K extends keyof T>(obj: T, key: K) {
   return obj[key];
}

const uuuser: IUser = {
   name: 'Jojo',
   age: 16
};

const uuuserName = getValue(uuuser, 'name');

// part.  typeof

let strOrNum: string | number = 5;

if (Math.random() > 0.5) {
   strOrNum = 5;
} else {
   strOrNum = 'string';
}

if (typeof strOrNum === 'string') {
   console.log(strOrNum);
} else {
   console.log(strOrNum);
}

let strOrNumber: typeof strOrNum;

const ktuser = {
   name: 'Mieruko',
   notName: 23
};

type keysOfKtuser = keyof typeof ktuser;

enum Direction {
   Up,
   Down
}

type d = keyof typeof Direction;

// part. indexed access types

interface IPermission {
   endDate: Date;
}

interface IRrrrole {
   name: string;
}

interface IUsssser {
   name: string,
   roles: IRrrrole[];
   permission: IPermission;
}

const uuuussssr: IUsssser = {
   name: 'Natsu',
   roles: [],
   permission: {
      endDate: new Date()
   }
};

const nameUsssr = uuuussssr['name'];
const roleName = 'roles';
let roleNamee: 'roles' = 'roles';


type rolesType = IUsssser['roles'];
type rolesType2 = IUsssser[typeof roleNamee];

type roleType = IUsssser['roles'][number];
type dateType = IUsssser['permission']['endDate'];

const roles = ['admin', 'user', 'super-user'] as const;
type roleTypes = typeof roles[number];