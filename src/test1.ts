const info: {
   officeId: number,
   isOpened: boolean,
   contacts: {
      phone: string,
      email: string,
      adress: {
         city: string;
      };
   };
} = {
   "officeId": 45,
   "isOpened": false,
   "contacts": {
      "phone": "0232322323",
      "email": "twentythree@gmail.com",
      "adress": {
         "city": "Kyiv"
      }
   }
}