// const https = require()

const sendedReq = {
   topicId: 23,
   status: "published"
}

enum QuestionStatus {
   Published = 'published',
   Deleted = 'deleted',
   Draft = 'draft'
};

async function getFaqs(req: {
   topicId: number,
   status?: QuestionStatus | string;
}): Promise<{
      "question": string,
      "answer": string,
      "tags": string[],
      "likes": number,
      "status": string
}[]> {
   const res = await fetch('/test2.json', {
      method: 'POST',
      body: JSON.stringify(req)
   });
   if (!res.ok) {
      throw new Error('Waaaaat?')
   }
   const data = await res.json();
   return data;
}

console.log(getFaqs(sendedReq));