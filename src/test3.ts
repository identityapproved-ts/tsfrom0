interface ITransaction {
   sum: number,
   from: number,
   to: number
};

enum TransactionStatus {
   Success = 'success',
   Failed = 'failed'
};

interface ITransactionRequest extends ITransaction { };

interface ITransactionSuccess extends ITransaction {
   "databaseId": number
};

interface ITransactionFailed {
   "errorMessage": string,
	"errorCode": number
};
/* 
interface IResponse {
   status: TransactionStatus;
   data: ITransactionSuccess | ITransactionFailed
}; // !wrong => status 'success' can be with failed data
 */
interface IResponseSuccess {
   status: TransactionStatus.Success;
   data: ITransactionSuccess
};

interface IResponseFailed {
   status: TransactionStatus.Failed;
   data:ITransactionFailed
};

const sendedRequest = {
   "sum": 10000,
   "from": 2,
   "to": 4
};

async function transaction(sendedRequest:ITransactionRequest): Promise<IResponseSuccess|IResponseFailed> {
   const res = await fetch('./test3.json', {
      method: 'POST',
      body: JSON.stringify(sendedRequest)
   });
   const data = await res.json();
   return data;
};