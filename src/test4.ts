interface ITransaction {
   sum: number,
   from: number,
   to: number
};

// enum StatusTransaction {
//    Success = 'success',
//    Failed = 'failed'
// };

interface ITransactionRequest extends ITransaction { };

interface ITransactionSuccess extends ITransaction {
   "databaseId": number
};

interface ITransactionFailed {
   "errorMessage": string,
	"errorCode": number
};

interface IResponseSuccess {
   condition: TransactionStatus.Success;
   data: ITransactionSuccess
};

interface IResponseFailed {
   condition: TransactionStatus.Failed;
   data:ITransactionFailed
};

// type StatusFunction = (res: IResponseSuccess | IResponseFailed) => number;

type Res = IResponseSuccess | IResponseFailed

function isSuccess(res: Res): res is IResponseSuccess {
   if (res.status === TransactionStatus.Success) {
      return true
   }
   return false
   // return (res as IResponseSuccess).status !== undefined;
}
function transactionStatus(res: Res): number {
   if (isSuccess(res)) {
      return res.data.databaseId
   } else {
      throw new Error(res.data.errorMessage);
   }
}