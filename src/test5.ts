/* Необходимо сделать корзину (Cart) на сайте,
которая имееет список продуктов (Product), добавленных в корзину
и переметры доставки (Delivery). Для Cart реализовать методы:
- Добавить продукт в корзину
- Удалить продукт из корзины по ID
- Посчитать стоимость товаров в корзине
- Задать доставку
- Checkout - вернуть что всё ок, если есть продукты и параметры доставки
Product: id, название и цена
Delivery: может быть как до дома (дата и адрес) или до пункта выдачи (дата = Сегодня и Id магазина) */

class Product {
  constructor(public id: number, public name: string, public price: number) { }
}

class Delivery {
  constructor(public date: Date) { }
}

class HomeDelivery extends Delivery {
  constructor(public adress: string, public override date: Date) {
    super(date);
  }
}

class ShopDelivery extends Delivery {
  constructor(public shopId: string) {
    super(new Date());
  }
}

type DeliveryOptions = HomeDelivery | ShopDelivery;

class Cart {
  private products: Product[] = [];
  private delivery: DeliveryOptions;

  public addProduct(product: Product): void {
    this.products.push(product);
  }

  public deleteProduct(productId: number): void {
    this.products = this.products.filter(
      (prod: Product) => productId !== prod.id
    );
  }

  public getSum(): number {
    return this.products
      .map((p: Product) => p.price)
      .reduce((p1: number, p2: number) => p1 + p2);
  }

  public setDelivery(delivery: DeliveryOptions): void {
    this.delivery = delivery;
  }

  public checkOut() {
    if (this.products.length === 0) {
      throw new Error("Add some product, please");
    }
    if (!this.delivery) {
      throw new Error("Add delivery options, please");
    }
    return { success: true };
  }
}

const cart = new Cart();
cart.addProduct(new Product(1, "Tofu", 2300));
cart.addProduct(new Product(2, "Soya", 2300));
cart.addProduct(new Product(3, "Rice", 9200));
cart.deleteProduct(1);
cart.setDelivery(new HomeDelivery("Lake", new Date()));
console.log(cart.getSum());
console.log(cart.checkOut());
