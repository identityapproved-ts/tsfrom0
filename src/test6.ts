/* Необходимо реализовать абстрактный класс Logger с 2-мя методами
абстрактным - log(message): void
printDate - выводящий в log дату
К нему необходимо сделать реальный класс, который бы имел метод: logWithDate,
выводящий сначала дату, а потом заданное сообщение */

abstract class AbsLogger {
  abstract logMessage(m: string): void;

  printDate(date: Date) {
    this.logMessage(date.toString());
  }
}

class DateLog extends AbsLogger {
  logMessage(m: string): void {
    console.log(m);
  }

  logWithDate(m: string) {
    this.printDate(new Date());
    this.logMessage(m);
  }
}

const logger = new DateLog();
logger.logWithDate("MESSAGE!");
