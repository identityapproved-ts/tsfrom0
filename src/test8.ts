/* Необходимо написать функцию группировки, которая принимает массив объектов
и его ключ, производит группировку по указанному ключу и возращает
сгруппированный объект. */


interface IData {
	group: number,
	name: string;
}


const groupData: IData[] = [
	{ group: 1, name: 'a' },
	{ group: 1, name: 'b' },
	{ group: 1, name: 'b' },
	{ group: 1, name: 'c' },
	{ group: 2, name: 'a' },
	{ group: 2, name: 'b' },
	{ group: 2, name: 'c' },
];

interface IGroup<T> {
	[key: string]: T[];
}

type key = string | number | symbol;

function group<T extends Record<key, any>>(array: T[], key: keyof T): IGroup<T> {
	return array.reduce<IGroup<T>>((map: IGroup<T>, item) => {
		const itemKey = item[key];
		let currEl = map[itemKey];
		console.log("~ itemKey", itemKey);

		if (Array.isArray(currEl)) {
			currEl.push(item);
		} else {
			currEl = [item];
		}

		map[itemKey] = currEl;
		console.log("~ currEl", currEl);
		return map;
	}, {});
}

const grouppedRes = group<IData>(groupData, 'name');
console.log(grouppedRes);