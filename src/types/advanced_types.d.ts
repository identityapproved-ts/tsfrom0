declare function logId(id: string | number | boolean): void;
declare function logErrors(err: string | string[]): void;
declare function logObj(obj: {
    a: number;
} | {
    b: number;
}): void;
declare function logMultiplesIds(a: string | number, b: string | boolean): void;
declare function fetchWithAuth(url: string, method: 'post' | 'get'): number;
declare let method: string;
declare type httpMethod = 'post' | 'get';
declare function authFetch(url: string, method: httpMethod): number;
declare type AliasUser = {
    firstName: string;
    lastName: string;
    city: string;
    age: number;
    skills: string[];
};
declare type userId = {
    id: number;
};
declare type userWithId = AliasUser & userId;
declare const oldUser: userWithId;
interface UserInterface {
    firstName: string;
    lastName: string;
    city: string;
    age: number;
    skills: string[];
    log: (id: number) => string;
}
interface UserCreated {
    createdAt: Date;
}
interface UserIdInterface extends UserInterface, UserCreated {
    id: number;
}
declare const newUser: UserIdInterface;
interface UserDictionaryInterface {
    [index: number]: UserInterface;
}
interface Ichigo {
    age: number;
}
interface Ichigo {
    bankai: string;
}
declare const ichigo: Ichigo;
interface OptionalUser {
    login: string;
    password?: string;
}
declare const optUser: OptionalUser;
declare function multiply(first?: number, second?: number): number;
interface UserPassType {
    login: string;
    password?: {
        type: 'primary' | 'secondary';
    };
}
declare function testPassword(user: UserPassType): void;
declare function testing(param?: number): void;
declare function lodSomething(smth: string | number): void;
declare const voidLog: void;
declare function mult(frst: number, scnd?: number): number;
declare type voidFunction = () => void;
declare const stands: string[];
declare const sortedList: {
    s: string[];
};
declare let input: unknown;
declare function run(i: unknown): void;
declare function getSmth(): Promise<void>;
declare type UnionUnknown = unknown | null;
declare type intersectionUnknown = unknown & null;
declare function generateError(message: string): never;
declare function dumpError(): never;
declare function recurs(): never;
declare type switchAction = 'commit' | 'checkout';
declare function switchProcess(action: switchAction): void;
declare function isString(x: string | number): boolean;
declare const n: null;
declare const n1: any;
interface IUser {
    name: string;
}
declare function getUser(): IUser | null;
declare const user: IUser | null;
declare let a: number;
declare let b: string;
declare let c: string;
declare let d: boolean;
interface User {
    name: string;
    email: string;
    login: string;
}
declare const bestUser: User;
interface Admin {
    name: string;
    role: number;
}
declare const admin: Admin;
declare function userToAdmin(user: User): Admin;
declare function log(id: string | number): void;
declare function isAString(x: string | number): x is string;
declare function isAdmin(user: User | Admin): user is Admin;
declare function isAdminAlternative(user: User | Admin): user is Admin;
declare function setRole(user: User | Admin): void;
//# sourceMappingURL=advanced_types.d.ts.map