declare const compute: () => number;
declare enum Roles {
    admin = 23,
    user
}
declare function test(a: {
    admin: number;
    user: number;
}): void;
declare const enum RolesCnst {
    admin = 23,
    user = 666
}
declare const res = RolesCnst.user;
//# sourceMappingURL=basic_types.d.ts.map