declare class OopUser {
    name: string;
    age: number;
    constructor();
    constructor(name: string);
    constructor(age: number);
    constructor(name: string, age: number);
}
declare const userClass: OopUser;
declare const userClass1: OopUser;
declare const userClass2: OopUser;
declare const userClass3: OopUser;
declare enum PaymentsStatus {
    Holded = 0,
    Processed = 1,
    Reversed = 2
}
declare class Payment {
    id: number;
    status: PaymentsStatus;
    createdAt: Date;
    updatedAt: Date;
    constructor(id: number);
    getPaymentLifeTime(): number;
    unholdPayment(): void;
}
declare const payment: Payment;
declare const time: number;
declare class userSetOrGet {
    _login: string;
    password: string;
    createdAt: Date;
    set login(l: string);
    get login(): string;
    setPassword(p: string): Promise<void>;
}
declare const userm: userSetOrGet;
interface ILogger {
    log(...args: any[]): void;
    error(...args: any[]): void;
}
declare class Logger implements ILogger {
    log(...args: any[]): void;
    error(...args: any[]): Promise<void>;
}
interface IPayable {
    pay(paymentId: number): void;
    price?: number;
}
interface IDeleteble {
    delete(): void;
}
declare class PayableUser implements IPayable, IDeleteble {
    delete(): void;
    pay(paymentId: number): void;
    price?: number;
}
declare type PaymentStatus = "new" | "paid";
declare class PaymentExt {
    id: number;
    status: PaymentStatus;
    constructor(id: number);
    pay(): void;
}
declare class PersistendPayment extends PaymentExt {
    databaseId: number;
    paidAt: Date;
    constructor();
    save(): void;
    pay(date?: Date): void;
}
declare class Write {
    process: string;
    constructor();
}
declare class Read extends Write {
    process: string;
    constructor();
}
declare class HttpError extends Error {
    code: number;
    constructor(message: string, code?: number);
}
declare class Usr {
    name: string;
    constructor(name: string);
}
declare class Usrs extends Array<Usr> {
    searchUser(name: string): Usr[];
    toString(): string;
}
declare const usrs: Usrs;
declare class UsrsList {
    users: Usr[];
    push(u: Usr): number;
}
declare class PaymentComp {
    date: Date;
}
declare class UserWithPayment {
    user: Usr;
    payment: PaymentComp;
    constructor(user: Usr, payment: PaymentComp);
}
declare class Vehicle {
    #private;
    mark: string;
    private damages;
    private _model;
    protected run: number;
    set model(m: string);
    get model(): string;
    isPriceEqual(p: Vehicle): boolean;
    addDamage(damage: string): void;
}
declare class Track extends Vehicle {
    setRun(km: number): void;
}
declare class UserService {
    static property: number;
    private static db;
    static getUser(id: number): Promise<void>;
    constructor(id: number);
    create(): void;
}
declare const inst: UserService;
declare class ThisPayment {
    private date;
    getDate(this: ThisPayment): Date;
    getDateArrow: () => Date;
}
declare const p: ThisPayment;
declare const thisUser: {
    id: number;
    paymentDate: () => Date;
    paymentDateArrow: () => Date;
};
declare class PaymentPersistant extends ThisPayment {
    save(): Date;
}
declare class UserBuilder {
    name: string;
    setName(name: string): this;
    isAdmin(): this is AdminBuilder;
}
declare class AdminBuilder extends UserBuilder {
    role: string[];
}
declare const usrBldr: UserBuilder;
declare const usrBldr2: AdminBuilder;
declare let usssr: UserBuilder | AdminBuilder;
declare abstract class Controller {
    abstract handle(req: any): void;
    handleWithLogs(req: any): void;
}
declare class UserController extends Controller {
    handle(req: any): void;
}
declare const usrContr: UserController;
//# sourceMappingURL=classes.d.ts.map