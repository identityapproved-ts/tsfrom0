declare const arrr: Array<number>;
declare function gen(): Promise<void>;
declare const check: Record<string, boolean>;
declare function logMiddleware<T>(data: T): T;
declare const logRes: number;
declare function splitHalf<T>(data: Array<T>): Array<T>;
declare function dataToString<T>(data: T): string | undefined;
declare const split: <T>(data: Array<T>) => Array<T>;
declare const split2: <Y>(data: Array<Y>) => Array<Y>;
interface ILogLine<T> {
    timeStamp: Date;
    data: T;
}
declare type LogLineType<T> = {
    timeStamp: Date;
    data: T;
};
declare const logLine: ILogLine<{
    a: number;
}>;
declare class Venicle {
    run: number;
}
declare function kmToMiles<T extends Venicle>(venicle: T): T;
declare class LCV extends Venicle {
    capacity: number;
}
declare const venicle: Venicle;
declare const lcv: LCV;
declare class Resp<D, E> {
    data?: D;
    error?: E;
    constructor(data?: D, error?: E);
}
declare class HTTPResp<F> extends Resp<string, number> {
    code: F;
    setCode(code: F): void;
}
declare const resp2: HTTPResp<unknown>;
declare type Consstructor = new (...args: any[]) => {};
declare type GConsstructor<T = {}> = new (...args: any[]) => T;
declare class List {
    items: string[];
    constructor(items: string[]);
}
declare class Accordion {
    isOpened: boolean;
}
declare type ListType = GConsstructor<List>;
declare type AccordionType = GConsstructor<Accordion>;
declare class ExtendedListClass extends List {
    first(): string;
}
declare function ExtendedList<TBase extends ListType & AccordionType>(Base: TBase): {
    new (...args: any[]): {
        first(): string;
        items: string[];
        isOpened: boolean;
    };
} & TBase;
declare class AccordionList {
    items: string[];
    isOpened: true;
    constructor(items: string[]);
}
declare const list: {
    new (...args: any[]): {
        first(): string;
        items: string[];
        isOpened: boolean;
    };
} & typeof AccordionList;
declare const ressss: {
    first(): string;
    items: string[];
    isOpened: boolean;
} & AccordionList;
//# sourceMappingURL=generics.d.ts.map