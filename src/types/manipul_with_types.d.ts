interface IUser {
    name: string;
    age: number;
}
declare type KeysOfUser = keyof IUser;
declare const key: KeysOfUser;
declare function getValue<T, K extends keyof T>(obj: T, key: K): T[K];
declare const uuuser: IUser;
declare const uuuserName: string;
//# sourceMappingURL=manipul_with_types.d.ts.map