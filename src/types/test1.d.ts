declare const info: {
    officeId: number;
    isOpened: boolean;
    contacts: {
        phone: string;
        email: string;
        adress: {
            city: string;
        };
    };
};
//# sourceMappingURL=test1.d.ts.map