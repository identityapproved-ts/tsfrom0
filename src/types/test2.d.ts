declare const sendedReq: {
    topicId: number;
    status: string;
};
declare enum QuestionStatus {
    Published = "published",
    Deleted = "deleted",
    Draft = "draft"
}
declare function getFaqs(req: {
    topicId: number;
    status?: QuestionStatus | string;
}): Promise<{
    "question": string;
    "answer": string;
    "tags": string[];
    "likes": number;
    "status": string;
}[]>;
//# sourceMappingURL=test2.d.ts.map