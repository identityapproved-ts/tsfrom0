interface ITransaction {
    sum: number;
    from: number;
    to: number;
}
declare enum TransactionStatus {
    Success = "success",
    Failed = "failed"
}
interface ITransactionRequest extends ITransaction {
}
interface ITransactionSuccess extends ITransaction {
    "databaseId": number;
}
interface ITransactionFailed {
    "errorMessage": string;
    "errorCode": number;
}
interface IResponseSuccess {
    status: TransactionStatus.Success;
    data: ITransactionSuccess;
}
interface IResponseFailed {
    status: TransactionStatus.Failed;
    data: ITransactionFailed;
}
declare const sendedRequest: {
    sum: number;
    from: number;
    to: number;
};
declare function transaction(sendedRequest: ITransactionRequest): Promise<IResponseSuccess | IResponseFailed>;
//# sourceMappingURL=test3.d.ts.map