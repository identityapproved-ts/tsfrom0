interface ITransaction {
    sum: number;
    from: number;
    to: number;
}
interface ITransactionRequest extends ITransaction {
}
interface ITransactionSuccess extends ITransaction {
    "databaseId": number;
}
interface ITransactionFailed {
    "errorMessage": string;
    "errorCode": number;
}
interface IResponseSuccess {
    condition: TransactionStatus.Success;
    data: ITransactionSuccess;
}
interface IResponseFailed {
    condition: TransactionStatus.Failed;
    data: ITransactionFailed;
}
declare type Res = IResponseSuccess | IResponseFailed;
declare function isSuccess(res: Res): res is IResponseSuccess;
declare function transactionStatus(res: Res): number;
//# sourceMappingURL=test4.d.ts.map