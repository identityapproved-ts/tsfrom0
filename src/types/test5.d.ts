declare class Product {
    id: number;
    name: string;
    price: number;
    constructor(id: number, name: string, price: number);
}
declare class Delivery {
    date: Date;
    constructor(date: Date);
}
declare class HomeDelivery extends Delivery {
    adress: string;
    date: Date;
    constructor(adress: string, date: Date);
}
declare class ShopDelivery extends Delivery {
    shopId: string;
    constructor(shopId: string);
}
declare type DeliveryOptions = HomeDelivery | ShopDelivery;
declare class Cart {
    private products;
    private delivery;
    addProduct(product: Product): void;
    deleteProduct(productId: number): void;
    getSum(): number;
    setDelivery(delivery: DeliveryOptions): void;
    checkOut(): {
        success: boolean;
    };
}
declare const cart: Cart;
//# sourceMappingURL=test5.d.ts.map