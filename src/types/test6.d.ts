declare abstract class AbsLogger {
    abstract logMessage(m: string): void;
    printDate(date: Date): void;
}
declare class DateLog extends AbsLogger {
    logMessage(m: string): void;
    logWithDate(m: string): void;
}
declare const logger: DateLog;
//# sourceMappingURL=test6.d.ts.map