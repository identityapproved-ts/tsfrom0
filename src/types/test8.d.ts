interface IData {
    group: number;
    name: string;
}
declare const groupData: IData[];
interface IGroup<T> {
    [key: string]: T[];
}
declare type key = string | number | symbol;
declare function group<T extends Record<key, any>>(array: T[], key: keyof T): IGroup<T>;
declare const grouppedRes: IGroup<IData>;
//# sourceMappingURL=test8.d.ts.map