declare const data: {
    id: number;
    name: string;
}[];
interface ID {
    id: number;
}
declare function sortingData<T extends ID>(data: T[], type?: 'asc' | 'desc'): T[];
//# sourceMappingURL=test9.d.ts.map